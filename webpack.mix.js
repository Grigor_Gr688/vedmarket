const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */



mix.styles([
    'resources/css/bootstrap.min.css',
    'resources/css/header-footer.css'
],'public/css/app.css');


mix.scripts([
    'resources/js/jquery-3.6.0.min.js',
    'resources/js/bootstrap.js',
    'resources/js/bootstrap.bundle.min.js',
    'resources/js/my-script.js'
],'public/js/bootstrap.bundle.min.js');


mix.copyDirectory("resources/img","public/img");


mix.styles([
    'resources/css/owl.carousel.min.css',
    'resources/css/owl.theme.default.min.css',
    'resources/css/animate.css'
],'public/plugins/css/owl.carousel.min.css');

mix.scripts([
    'resources/js/owl.carousel.min.js',
    'resources/js/wow.min.js'
],'public/plugins/js/owl.carousel.min.js');


mix.styles('resources/css/index.css','public/css/index.css');
