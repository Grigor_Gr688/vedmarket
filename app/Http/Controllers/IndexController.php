<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){

        $page_name = "Главная";
        return view("web.index",compact('page_name'));
    }
}
