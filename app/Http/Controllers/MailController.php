<?php

namespace App\Http\Controllers;

use App\Mail\FreeMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class MailController extends Controller
{
    public function freeSend(Request $request){

        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'tel_number'=>'required|numeric',
            'email'=>'required|email',
        ]);

        if($validator->fails()){
            return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()
            ));
        }else{
            $body = "<h1>Нужна бесплатная консультация</h1>";
            $body .= "<p><span style='font-size:17px;font-weight:bold'>Имя: </span><span style='font-size:15px'>{$request->input('name')}<span></p>";
            $body .= "<p><span style='font-size:17px;font-weight:bold'>Телефон: </span><span style='font-size:15px'>{$request->input('tel_number')}<span></p>";
            $body .= "<p><span style='font-size:17px;font-weight:bold'>Почта: </span><span style='font-size:15px'>{$request->input('email')}<span></p>";
            Mail::to('site.profved@yandex.ru')->send(new FreeMail($body));

            return response()->json(array(
                'success'=>true,
                'message'=>'Отправлено !'
            ));
        }
    }

    public function writeToMail(Request $request){
        dd($request);
        $body = "<p><span style='font-size:17px;font-weight:bold'>Телефон: </span><span style='font-size:15px'>{$request->input('telephone')}<span></p>";
        $body .= "<p><span style='font-size:17px;font-weight:bold'>Почта: </span><span style='font-size:15px'>{$request->input('name')}<span></p>";
        $body .= "<p><span style='font-size:17px;font-weight:bold'>Почта: </span><span style='font-size:15px'>{$request->input('email')}<span></p>";
        $body .= "<p><span style='font-size:17px;font-weight:bold'>Сообщение: </span><span style='font-size:15px'>{$request->input('message')}<span></p><br>";
        Mail::to('site.profved@yandex.ru')->send(new FreeMail($body));

        return response()->json('Отправлено !');
    }

    public function discoverPrise(Request $request){
        $body = "<p><span style='font-size:17px;font-weight:bold'>Имя: </span><span style='font-size:15px'>{$request->input('anun')}<span></p>";
        $body .= "<p><span style='font-size:17px;font-weight:bold'>Номер: </span><span style='font-size:15px'>{$request->input('nomer')}<span></p>";
        Mail::to('site.profved@yandex.ru')->send(new FreeMail($body));

        return response()->json('Отправлено !');
    }
}
