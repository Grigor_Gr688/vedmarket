$(document).ready(function(){
    if($("div").is('.slider')){
        $(".slider").owlCarousel({
            items:1,
            loop:true,
            autoplay:3000,
            smartSpeed:750,
            dots:false,
        });
    }

    if($("div").is('.slider-company')){
        $(".slider-company").owlCarousel({
            loop:true,
            autoplay:3000,
            smartSpeed:750,
            dots:false,
            margin:10,
            responsiveClass:true,
            center: true,
            dots:true,
            responsive:{
                0:{
                    items:1,
                },
                550:{
                    items:2,
                },
                1000:{
                    items:3,
                }
            }
        });
    }
});


$('.nam-title').click(function (){
    $('.nam-content').slideToggle();
});


$("#check_1").click(function (){
    if($(this).prop("checked")){
        $(".get-consultation").prop('disabled',false);
    }else{
        $(".get-consultation").prop('disabled',true);
    }
});



//
//
//
// AJAX //
//
//
//

function show_toggle(){
    $(".loader").show();
    $("#check_1").prop("checked",false);
    $("#check_1").prop("disabled",true);
    $(".get-consultation").prop('disabled',true);
}

function load_empty(){
    $(".loader").hide();
    $("#check_1").prop("disabled",false);
    $("div.feed-size").text("");
}

function result(info){
    load_empty();
    $('input.form-controls').val("");
    document.getElementById('result_success').innerHTML = info;
    setTimeout(()=>{
        document.getElementById('result_success').innerHTML = "";
    },3000);
}

function errors(info){
    load_empty();
    for(var key in info){
        for (const error of info[key]){
            $("#consultation input[name="+key+"]").next().append("<div>"+error+"</div>");
        }
    }
}

let form = document.getElementById('consultation');

if(form){
    form.addEventListener('submit', function(event){
        show_toggle();
        const form = new FormData(this);

        let promise = fetch(url, {method: 'POST', body:form });

        promise.then(response=>{
            return response.json();
        }).then(text=>{
            if(text.success){
                $(".loader").hide();
                result(text.message);
            }else{
                errors(text.errors);
            }
        });

        event.preventDefault();
    });
}


//
//
// Напишите нам
//
//
//


const valid = document.querySelectorAll(".valid");

valid.forEach(index=>{
    index.oninput = () =>{
        if((valid1.value != "" && valid1.value.length > 5) &&
        (valid2.value != "" && valid2.value.length > 8) &&
        (valid3.value != "" && valid3.value.length > 8)){
            document.getElementById("send_btn").disabled = false;
        }else{
            document.getElementById("send_btn").disabled = true;
        }
    }
});

function nam_result(result){
    $(".nam-cover>img").css({"display":"none"});
    $(".nam-cover>span").text(result);
    $(".nam-cover>span").css({"display":"inline"});
    $("#send_btn").prop("disabled",true);
    $('.valid').val("");
    setTimeout(() =>{
        $(".nam-cover").css({"display":"none"});
        $(".nam-cover>img").css({"display":"block"});
        $(".nam-cover>span").css({"display":"none"});
        $('.nam-content').slideToggle();
    },2000);
}

let send = document.getElementById("nam");

if(send){
    send.addEventListener("submit", function(event){
        $(".nam-cover").css({"display":"flex"});

        let promise = fetch(url1,{ method:"POST",body:new FormData(this) });

        promise.then(response=>{
            return response.json();
        }).then(result=>{
            nam_result(result);
        });

        event.preventDefault();
    });

}



//
//
// Узнать стоимость
//
//


function nam_result1(result){
    $(".modal-body .nam-cover>img").css({"display":"none"});
    $(".modal-body .nam-cover>span").text(result);
    $(".modal-body .nam-cover>span").css({"display":"inline"});
    $("form#prise input").val("");
    $("form#prise button").prop("disabled",true);
    setTimeout(() =>{
        $(".modal-body .nam-cover").css({"display":"none"});
        $(".modal-body .nam-cover>img").css({"display":"block"});
        $(".modal-body .nam-cover>span").css({"display":"none"});
    },2000);
}

$("form#prise input").on("input", function (){
    if(anun.value != "" && nomer.value != ""){
        $("form#prise button.next-read").prop("disabled",false);
    }else{
        $("form#prise button.next-read").prop("disabled",true);
    }
});

let prise = document.getElementById("prise");

prise.addEventListener("submit", function(event){
    $(".modal-content .nam-cover").css({"display":"flex"});

    let promise = fetch(url2,{method:"POST",body:new FormData(this)});

    promise.then(response=>{
        return response.json();
    }).then(result=>{
        console.log(result);
        nam_result1(result);
    });

    event.preventDefault();
});
