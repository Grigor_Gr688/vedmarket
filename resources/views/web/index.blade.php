@extends('layouts.layout')

@section('styles')

    <link rel="stylesheet" href="{{asset('plugins/css/owl.carousel.min.css')}}">
@endsection

@section('content')
    <div class="owl-carousel owl-theme slider">
        <div>
            <img src="{{asset('img/slider.png')}}" />
        </div>
    </div>

    <div class="my-container shinarar my-5">
        <div>
            <h1>О компании</h1>
            <div class="shinarar-text">ООО «Профвэдмаркет»  осуществляет аутсорсинг в сфере международных перевозок грузов.
                Наши специалисты являются профессионалами в сфере грузоперевозок и таможенного оформления с многолетним опытом.
                Ежемесячно мы возим до 1200 тн груза и оформляем до 300 деклараций.
            </div>

            <h1>Миссия</h1>
            <div class="shinarar-text">Мы работаем для того, чтобы наши клиенты могли развиваться и расти на международном рынке.
                <br>
                <h5>Наши ценности</h5>
                <ul>
                    <li>Мы ставим клиентов на первое место.</li>
                    <li>Открытость и честность – мы работаем без скрытых комиссий. Мы честны с клиентами и партнерами.</li>
                    <li>Бережливость – делать больше с меньшими затратами.</li>
                    <li>Приверженность команде – мы помогаем и поддерживаем друг друга, создаем позитивный и семейный дух.</li>
                </ul>
            </div>
            <a href="{{route('aboutCompany')}}"><button class="next-read">Читать далее</button></a>
        </div>
        <img src="{{asset('img/shinarar-kes.png')}}" title="Надежная и быстрая доставка" />
    </div>

    <div class="uslugi-block">
        <div>
            <h2 class="uslugi">УСЛУГИ</h2>
            <div></div>
        </div>
    </div>

    <div class="my-container uslugi-icons mt-5">
        <div class="row">
            <div class="col-sm-6 col-xs-8 mr-sm-1 mr-xs-0 my-3 mx-auto wow fadeInLeft">
                <a href="{{route('services',['item'=>1])}}" class="d-flex align-items-center justify-content-center" style="width:100%;">
                    <img src="{{asset('img/autsorsing.png')}}" />
                    <p>Аутсорсинг ВЭД</p>
                </a>
            </div>
            <div class="col-sm-6 col-xs-8 mr-sm-1 mr-xs-0 my-3 mx-auto wow fadeInRight">
                <a href="{{route('services',['item'=>2])}}" class="d-flex align-items-center justify-content-center" style="width:100%;">
                    <img src="{{asset('img/sertifikacya.png')}}" />
                    <p>Таможенное оформление</p>
                </a>
            </div>
            <div class="col-sm-6 col-xs-8 mr-sm-1 mr-xs-0 my-3 mx-auto wow fadeInLeft" data-wow-delay="0.5s">
                <a href="{{route('services',['item'=>3])}}" class="d-flex align-items-center justify-content-center" style="width:100%;">
                    <img src="{{asset('img/dostavka-kitay.png')}}" />
                    <p>Доставка грузов из Китая</p>
                </a>
            </div>
            <div class="col-sm-6 col-xs-8 mr-sm-1 mr-xs-0 my-3 mx-auto wow fadeInRight" data-wow-delay="0.5s">
                <a href="{{route('services',['item'=>4])}}" class="d-flex align-items-center justify-content-center" style="width:100%;">
                    <img src="{{asset('img/poisk.png')}}" />
                    <p>Поиск поставщиков товара</p>
                </a>
            </div>
            <div class="col-sm-6 col-xs-8 mr-sm-1 mr-xs-0 my-3 mx-auto wow fadeInUp" data-wow-delay="1s">
                <a href="{{route('services',['item'=>5])}}" class="d-flex align-items-center justify-content-center" style="width:100%;">
                    <img src="{{asset('img/dostavka.png')}}" />
                    <p>Доставка грузов из Европы</p>
                </a>
            </div>
        </div>
    </div>

    <div class="free-consultation my-5" style="background:url('{{asset('img/map.png')}}');background-size:cover">
        <div class="my-5 form-block">
            <h2 class="mt-4 mb-3">Нужна бесплатная консультация?</h2>
            <p>Менеджеры ответят на ваши вопросы и произведут расчет стоимости услуг</p>
            <a name="free"></a>
            <form id="consultation">
                @csrf
                <div class="form-inputs mt-4">
                    <input type="text" placeholder="Ваше имя" name="name" class="form-controls" />
                    <div class="feedback feed-size"></div>
                    <input type="text" placeholder="Телефон" name="tel_number" class="form-controls mt-3" />
                    <div class="feedback feed-size"></div>
                    <input type="email" placeholder="Е-майл" name="email" class="form-controls mt-3" />
                     <div class="feedback feed-size"></div>
                </div>
                <div class="mt-5" align=center>
                    <img src="{{asset('img/loader.gif')}}" class="loader" />
                    <b><div class="text-success" id="result_success"></div></b>
                </div>
                <div class="submit-btn mt-3">
                    <button type="submit" class="get-consultation" disabled>Получить консультацию</button>
                </div>
            </form>
            <div class="ya-prinimau mt-5 mb-3">
                <div class="check-block">
                    <input type="checkbox" name="Checkbox" id="check_1">
                    <label for="check_1"></label>
                </div>
                <span>Я принимаю условия обработки <span class="text-danger">персональных данных</span></span>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script> const url = @json(route('freeSend')); </script>
    <script src="{{asset('plugins/js/owl.carousel.min.js')}}"></script>
    <script> new WOW().init(); </script>
@endsection
