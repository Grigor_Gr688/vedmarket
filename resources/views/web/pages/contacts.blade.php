@extends('layouts.layout')

@section('content')
    <div class="my-container my-4">
        <div>Адрес: 660125, г. Красноярск, ул. Шумяцкого, дом 10</div>
        <div>Почта: info@profved.market</div>
        <p>Телефон: +7 905 788-36-67</p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2226.9935828070056!2d92.92947171562261!3d56.070721980639576!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5cd7af48506aae3b%3A0xa810b5a9b1b49da!2z0YPQuy4g0KjRg9C80Y_RhtC60L7Qs9C-LCAxMCwg0JrRgNCw0YHQvdC-0Y_RgNGB0LosINCa0YDQsNGB0L3QvtGP0YDRgdC60LjQuSDQutGA0LDQuSwg0KDQvtGB0YHQuNGPLCA2NjAxMTg!5e0!3m2!1sru!2s!4v1618304778481!5m2!1sru!2s" width="100%" height="500" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>
@endsection
