@extends('layouts.layout')

@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/css/owl.carousel.min.css')}}">
@endsection

@section('content')

<div class="my-container mt-3">
    <div class="owl-carousel owl-theme slider-company">
        <div>
            <img src="{{asset('img/company-1.jpg')}}" title="Надежная команда по перевозкам" />
        </div>
        <div>
            <img src="{{asset('img/company-2.jpg')}}" title="Надежная команда по перевозкам" />
        </div>
        <div>
            <img src="{{asset('img/company-4.jpg')}}" title="Надежная команда по перевозкам" />
        </div>
        <div>
            <img src="{{asset('img/company-5.jpg')}}" title="Надежная команда по перевозкам" />
        </div>
        <div>
            <img src="{{asset('img/company-6.jpg')}}" title="Надежная команда по перевозкам" />
        </div>
        <div>
            <img src="{{asset('img/company-10.jpg')}}" title="Надежная команда по перевозкам" />
        </div>
    </div>
</div>

    <div class="my-container my-5">
        <h2>Миссия</h2>
        <p>Мы работаем для того, чтобы наши клиенты могли развиваться и расти на международном рынке.

        <h6>Наши ценности</h6>
        <ul>
            <li>Мы ставим клиентов на первое место.</li>
            <li>Открытость и честность – мы работаем без скрытых комиссий. Мы честны с клиентами и партнерами.</li>
            <li>Бережливость – делать больше с меньшими затратами.</li>
            <li>Приверженность команде – мы помогаем и поддерживаем друг друга, создаем позитивный и семейный дух.</li>
        </ul>
        <h2>О компании</h2>

        <p>ООО «Профвэдмаркет»  осуществляет аутсорсинг в сфере международных перевозок грузов.
            Наши специалисты являются профессионалами в сфере грузоперевозок и таможенного оформления с многолетним опытом.
            Ежемесячно мы возим до 1200 тн груза и оформляем до 300 деклараций.</p>
    </div>
@endsection

@section("scripts")
    <script src="{{asset('plugins/js/owl.carousel.min.js')}}"></script>
@endsection
