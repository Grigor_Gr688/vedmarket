<footer>

    <div class="footer-header">
        <img src="{{asset('img/truck.png')}}" />
        <div class="footer-info-items">
            <div>
                <img src="{{asset('img/phone-rotate.png')}}" />
                <div class="item-title">Контакты</div>
                <div>+7 905 788-36-67</div>
            </div>
            <div>
                <img src="{{asset('img/time.png')}}" />
                <div class="item-title">Рабочее время</div>
                <div>Пн-Пят</div>
                <div>09:00-18:00</div>
            </div>
            <div>
                <img src="{{asset('img/geo-geo.png')}}" />
                <div class="item-title">Адрес</div>
                <div>Шумяцкого 10</div>
                {{-- <div>Научный проезд, 19, 7 этаж, офис 5</div> --}}
            </div>
        </div>
    </div>

    <div class="footer-content">
        <div class="footer-nkar">
            <img src="{{asset('img/logo.png')}}" />
        </div>
        <div class="muq">
            <a href="{{route('aboutCompany')}}"><div>Компания</div></a>
            <a href="{{route('index')}}"><div>Главная</div></a>
            <a href="{{route('aboutCompany')}}"><div>О компани</div></a>
            <a href="{{route('services',['item'=>1])}}"><div>Услуги</div></a>
            <a href="{{route('information')}}"><div>Информация</div></a>
            <a href="{{route('tariffs')}}"><div>Тарифы</div></a>
            <a href="{{route('contacts')}}"><div>Контакты</div></a>
            <a><div type="button" data-toggle="modal" data-target="#exampleModalCenter">Узнать стоимость</div></a>
        </div>
        <div>
            <div>НАШ ОФИС</div>
            {{-- <div>г. Красноярск</div> --}}
	        <div> ул. Шумяцкого 10</div>
            {{-- <div>Научный проезд, 19, 7 этаж, офис 5</div> --}}
            <div>+7 905 788-36-67</div>
            <div>info@profved.market</div>
        </div>
        <div>
            <div><a href="https://www.instagram.com/ved.market/?igshid=yh3dd46yzvzt">Instagram</a></div>
        </div>
    </div>
</footer>
<div class="napishite-nam">
    <div class="nam-title">
        <img src="{{asset('img/ket.png')}}" />
        <div>Напишите нам</div>
    </div>
    <div class="nam-content">
        <form id="nam">
            @csrf
            <textarea placeholder="Сообшения" name="message" class="valid" id="valid1"></textarea>
            <input type="text" placeholder="Имя" name="name" class="valid" />
            <input type="text" placeholder="Телефон" name="telephone" class="valid" id="valid2" onkeypress="return (event.charCode >= 48 && event.charCode <= 57 && /^\d{0,20}$/.test(this.value));" />
            <input type="email" placeholder="Емайл" name="email" class="valid" id="valid3" />
            <button type="submit" disabled id="send_btn">Отправить</button>
        </form>
        <div class="nam-cover">
            <img src="{{asset('img/loader.gif')}}" alt="">
            <span class="success_resolve"></span>
        </div>
    </div>
</div>
