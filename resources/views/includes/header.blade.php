<header>
    <div class="my-container py-2 my-container-header">
        <div class="header-title">
            <div class="logo">
                <a href="{{route('index')}}"><img src="{{asset('img/logo.png')}}" /></a>
            </div>
            <div class="info-blocks">
                <div class="hasce">
                    <div><img src="{{asset('img/geo-icon.png')}}" /></div>
                    <div>
                        <div>Шумяцкого 10</div>
                        {{-- <div></div> --}}
                    </div>
                </div>
                <div class="hamar">
                    <div><img src="{{asset('img/phone-icon.png')}}" /></div>
                    <div>
                        <div>Пн-Пят 09:00-18:00</div>
                        <div class="dark">+7 905 788-36-67</div>
                    </div>
                </div>
            </div>
        </div>
            <div class="nav-block">
            <div class="navigation">
                <nav class="navbar navbar-expand-lg navbar-light px-3 bg-light my-nav">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-nav">
                        <li class="nav-item active">
                        <a class="nav-link" href="{{route('index')}}">Главная</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{route('aboutCompany')}}">О компании</a>
                        </li>
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Услуги
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{route('services',['item'=>1])}}">Аутсорсинг ВЭД</a>
                            <a class="dropdown-item" href="{{route('services',['item'=>2])}}">Таможенное оформление</a>
                            <a class="dropdown-item" href="{{route('services',['item'=>3])}}">Доставка грузов из Китая</a>
                            <a class="dropdown-item" href="{{route('services',['item'=>4])}}">Поиск поставщиков товара</a>
                            <a class="dropdown-item" href="{{route('services',['item'=>5])}}">Доставка грузов из Европы</a>
                        </div>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{route('information')}}">Информация</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('tariffs')}}">Тарифы</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('contacts')}}">Контакты</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link li-prise" data-toggle="modal" data-target="#exampleModalCenter">Узнать стоимость</a>
                        </li>
                    </ul>
                    </div>
                </nav>

                <div type="button" class="prise px-3 py-sm-2 py-md-3" data-toggle="modal" data-target="#exampleModalCenter">Узнать стоимость</div>
            </div>

            <div class="page-name-block">
                <div>
                    <div><img src="{{asset('img/home-icon.png')}}" /></div>
                    <div class="page-name">{{$page_name}}</div>
                </div>
                <button class="navbar-toggler but" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <div></div>
                    <div style="margin: 5px 0px"></div>
                    <div></div>
                </button>
            </div>
        </div>
    </div>
</header>

  <!-- Modal -->
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" style="width:290px;margin: 0px auto;" role="document">
      <div class="modal-content" style="overflow:hidden">
        <div class="modal-header m-header">
          <div class="form-group">
            <h5 class="modal-title text-center" id="exampleModalLongTitle">Позвоним и ответим на все вопросы</h5>
          </div>
        </div>
        <div class="modal-body">
            <div class="nam-cover">
                <img src="http://vedmarket/img/loader.gif" alt="">
                <span class="success_resolve"></span>
            </div>
            <form id="prise">
                @csrf
                <div class="form-group">
                    <label for="anun" style="color:#968f9a">Ваше имя</label>
                    <input type="text" id="anun" name="anun" placeholder="Ваше имя" />
                </div>
                <div class="form-group">
                    <label for="nomer" style="color:#968f9a">Ваш номер</label>
                    <input type="text" id="nomer" name="nomer" placeholder="Ваш номер" onkeypress="return (event.charCode >= 48 && event.charCode <= 57 && /^\d{0,20}$/.test(this.value));" />
                </div>
                <button type="submit" class="next-read" disabled>Отправить</button>
            </form>
        </div>
      </div>
    </div>
  </div>
