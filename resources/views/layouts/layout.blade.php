<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>

        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" href="{{asset('css/index.css')}}">

        @yield('styles')

    </head>
    <body>
        @include('includes.header')

        @yield('content')

        @include('includes.footer')

        <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
        <script> const url1 = @json(route('writeToMail')); </script>
        <script> const url2 = @json(route('discoverPrise')); </script>
        @yield('scripts')
    </body>
</html>
