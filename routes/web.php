<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get("/",'IndexController@index')->name('index');

Route::get('/services/{item}',"PagesController@services")->name('services');
Route::get('/aboutCompany',"PagesController@aboutCompany")->name('aboutCompany');
Route::get('/information',"PagesController@information")->name('information');
Route::get('/tariffs',"PagesController@tariffs")->name('tariffs');
Route::get('/contacts',"PagesController@contacts")->name('contacts');

Route::post('/freeSend','MailController@freeSend')->name('freeSend');
Route::post('/writeToMail','MailController@writeToMail')->name('writeToMail');
Route::post('/discoverPrise','MailController@discoverPrise')->name('discoverPrise');
